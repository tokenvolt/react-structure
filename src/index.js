import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

const NotFound = () => <div>404</div>

const Mount = props => (
  <Router>
    <Route render={({ location }) => (
      location.state && location.state.is404
        ? <NotFound />
        : <App />
    )} />
  </Router>
)

ReactDOM.render(<Mount />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
