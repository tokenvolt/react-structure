import React, { Component } from 'react'
import { Route, Switch, Link, Redirect } from "react-router-dom";
import { NotFound } from 'shared/NotFound'

class Login extends Component {
  componentWillUnmount() {
    console.log("login unmounted")
  }

  render() {
    console.log("login")
    return (
      <Switch>
        <Route exact path={this.props.match.url} render={props => <div>login</div>} />
        <Route component={NotFound} />
      </Switch>
    )
  }
}

export { Login }
