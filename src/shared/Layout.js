/*
  DEPRECATED
**/

import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch, Redirect, Link } from "react-router-dom";
import { NotFound } from 'shared/NotFound'

const GeneralLayout = (props) => (
  <div aa={console.log(props)}>
    <div>current route :: {props.match.url}</div>
    <div>
      <Link to="/login">login</Link>
    </div>
    <div>
      <Link to="/payment">payment</Link>
    </div>
    <div>{props.children}</div>
  </div>
)

const AdminLayout = (props) => (
  <div>
    <div>ADMIN</div>
    <div>{props.children}</div>
  </div>
)

const ExpertsLayout = (props) => (
  <div>
    <div>EXPERTS</div>
    <div>{props.children}</div>
  </div>
)

const LoginLayout = (props) => (
  <div>
    <div>
      Authorize, please
    </div>
    <div>
      <Link to="/login">login</Link>
    </div>
    <div>
      <Link to="/payment">payment</Link>
    </div>
    <div>{props.children}</div>
  </div>
)

const SubLayout = ({ path, component: Component, children, ...rest }) => (
  <Route
    path={path}
    children={props => (
      <Component {...props}>
        {children}
      </Component>
    )}
    {...rest}
  />
)

export const Layout = props => (
  <Switch>
    <SubLayout path="/login" component={LoginLayout} {...props} />
    <SubLayout path="/admin" component={AdminLayout} {...props} />
    <SubLayout path="/experts" component={ExpertsLayout} {...props} />
    <SubLayout path="/" component={GeneralLayout} {...props} />
    <Route component={NotFound} />
  </Switch>
)


export const App = props => (
  <Switch>
    <SubLayout path="/login" component={LoginLayout} {...props} />
    <SubLayout path="/admin" component={AdminLayout} {...props} />
    <SubLayout path="/experts" component={ExpertsLayout} {...props} />
    <SubLayout path="/" component={GeneralLayout} {...props} />
    <Route component={NotFound} />
  </Switch>
)
