import React from 'react'
import { Redirect } from "react-router-dom";

export const NotFound = ({ location }) =>
  <Redirect to={Object.assign({}, location, { state: { is404: true } })} />