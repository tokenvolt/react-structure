import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import { Login } from 'pages/Login'
import { Payment } from 'pages/Payment'
// import { Projects } from 'pages/Projects'
// import { Layout } from 'shared/Layout'
import { NotFound } from 'shared/NotFound'

const Root = () => <div>root</div>

const GeneralL = (props) => (
  <div>
    <div>current route :: {props.match.url}</div>
    <div>
      <Link to="/login">login</Link>
    </div>
    <div>
      <Link to="/payment">payment</Link>
    </div>
    <Switch>
      <Route exact path="/" component={Root} />
      <Route exact path="/payment" component={Payment} />
    </Switch>
  </div>
)

const LoginL = (props) => (
  <div>
    <div>
      Authorize, please
    </div>
    <div>
      <Link to="/login">login</Link>
    </div>
    <div>
      <Link to="/payment">payment</Link>
    </div>
    <Route path="/login" component={Login} />
  </div>
)

const Layout = ({ path, component: Component, children, ...rest }) => (
  <Route
    path={path}
    children={props => (
      <Component {...props}>
        {children}
      </Component>
    )}
    {...rest}
  />
)

const App = props => (
  <Switch>
    <Layout path="/login" component={LoginL} />
    <Layout path="/" component={GeneralL} />
    <Route component={NotFound} />
  </Switch>
)

export default App
